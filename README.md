# unique-agg

Returns the unique not null value OR raise an exception



language: ![English](https://raw.githubusercontent.com/codenautas/multilang/master/img/lang-en.png)
also available in:
[![Spanish](https://raw.githubusercontent.com/codenautas/multilang/master/img/lang-es.png)](LEEME.md)

# Case of use

When you need to normalize the content of an imported table, `unique_agg` checks the data while inserting in the new table.

For example you have `imported_data` tabla and want to extract info for `countries` and `cities` tables.


```sql
INSERT INTO countries (country_id, country_name)
  SELECT country_id, unique_agg(country_name)
    FROM imported_data
    GROUP BY country_id;
```

# Install


```sh
psql < bin/create_unique_agg.sql
```


## License


[MIT](LICENSE)

