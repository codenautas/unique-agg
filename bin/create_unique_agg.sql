CREATE OR REPLACE FUNCTION unique_agg_raise(p1 anyelement, p2 anyelement) RETURNS anyelement
  LANGUAGE PLpgSQL
AS
$BODY$
BEGIN
  RAISE 'ERROR "%" DIF "%"', p1, p2;
END;
$BODY$;

CREATE OR REPLACE FUNCTION unique_agg_agg(p1 anyelement, p2 anyelement) RETURNS anyelement
  LANGUAGE SQL
AS
$SQL$
  SELECT CASE WHEN p1 IS NULL THEN p2 WHEN p2 IS NULL OR p1 = p2 THEN p1 ELSE unique_agg_raise(p1, p2) END;
$SQL$;

DROP AGGREGATE IF EXISTS unique_agg(anyelement);

CREATE AGGREGATE unique_agg(anyelement) (
    sfunc = unique_agg_agg,
    stype = anyelement
);

