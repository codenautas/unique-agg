<!--multilang v0 es:LEEME.md en:README.md -->
# unique-agg

<!--lang:es-->
Devuelve el único valor o lanza una excepción
<!--lang:en--]
Returns the unique not null value OR raise an exception

[!--lang:*-->

<!--multilang buttons-->

idioma: ![castellano](https://raw.githubusercontent.com/codenautas/multilang/master/img/lang-es.png)
también disponible en:
[![inglés](https://raw.githubusercontent.com/codenautas/multilang/master/img/lang-en.png)](README.md)

<!--lang:es-->
# Caso de uso

Al pasar datos de una tabla que no está normalizada hay que controlar que los datos estén bien en la tabla de origen.
El pasaje y el control se pueden hacer en un solo paso gracias a `unique_agg`. 

Por ejemplo si tenemos una tabla `imported_data` y queremos pasar los datos a `countries` y `cities`

<!--lang:en--]
# Case of use

When you need to normalize the content of an imported table, `unique_agg` checks the data while inserting in the new table. 

For example you have `imported_data` tabla and want to extract info for `countries` and `cities` tables. 

[!--lang:*-->

```sql
INSERT INTO countries (country_id, country_name)
  SELECT country_id, unique_agg(country_name)
    FROM imported_data
    GROUP BY country_id;
```

<!--lang:es-->
# Instalación

<!--lang:en--]
# Install

[!--lang:*-->

```sh
psql < bin/create_unique_agg.sql
```

<!--lang:es-->

## Licencia

<!--lang:en--]

## License

[!--lang:*-->

[MIT](LICENSE)

