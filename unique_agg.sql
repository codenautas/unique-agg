CREATE OR REPLACE FUNCTION unique_agg_raise(p1 anyelement, p2 anyelement) RETURNS anyelement
  LANGUAGE PLpgSQL
AS
$BODY$
BEGIN
  RAISE 'ERROR "%" DIF "%"', p1, p2;
END;
$BODY$;

CREATE OR REPLACE FUNCTION unique_agg_agg(p1 anyelement, p2 anyelement) RETURNS anyelement
  LANGUAGE SQL
AS
$SQL$
  SELECT CASE WHEN p1 IS NULL THEN p2 WHEN p2 IS NULL OR p1 = p2 THEN p1 ELSE unique_agg_raise(p1, p2) END;
$SQL$;

DROP AGGREGATE IF EXISTS unique_agg(anyelement);

CREATE AGGREGATE unique_agg(anyelement) (
    sfunc = unique_agg_agg,
    stype = anyelement
);

--TEST

do language plpgsql 
$sqls$
declare
  v_clean_test boolean:=true;
  v_int integer;
  v_text text;
  v_date date;
  v_other text;
begin
  -- prepare fixture
  DROP TABLE IF EXISTS test_table_unique_agg;
  CREATE TABLE test_table_unique_agg(
    an_int integer,
    a_text text,
    a_date date,
    other_text text
  );
  INSERT INTO test_table_unique_agg (an_int, a_text, a_date) values
    (1, 'hi', '2017-08-01'),
    (2, 'hi', '2017-08-01'),
    (2, 'hi', null);
    select unique_agg(a_text) into v_text
    from test_table_unique_agg;
  -- test
  if v_text is distinct from 'hi' then
    raise 'bad v_text %', v_text;
  end if;
  select unique_agg(a_date) into v_date
    from test_table_unique_agg;
  if v_date is distinct from '2017-08-01'::date then
    raise 'bad v_date %', v_date;
  end if;
  select unique_agg(other_text) into v_other
    from test_table_unique_agg;
  if v_other is not null then
    raise 'bad v_other %', v_other;
  end if;
  begin
    select unique_agg(an_int) into v_int
      from test_table_unique_agg;
    raise 'must raise an error' using ERRCODE = 'assert_failure';
  exception
    when raise_exception then
      if sqlstate <> 'P0001' then
        raise;
      end if;
  end;
  -- clean fixture
  if v_clean_test then
    DROP TABLE IF EXISTS test_table_unique_agg;
  end if;
end;
$sqls$;

